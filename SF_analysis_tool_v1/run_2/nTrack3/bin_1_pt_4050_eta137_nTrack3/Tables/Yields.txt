 |       | N_{jets}_M_1b | N_{jets}_nM_1b | 
 | #it{e#rightarrow#tau} | 8.27438 pm 0 | 17.9902 pm 0 | 
 | #it{h#rightarrow#tau} | 38.9261 pm 0 | 134.797 pm 0 | 
 | #it{#mu#rightarrow#tau} | 18.2565 pm 0 | 25.1028 pm 0 | 
 | #it{#tau} | 76.9401 pm 0 | 23.4073 pm 0 | 
 | Total | 142.397 pm 0 | 201.298 pm 0 | 
 | Data | 179 | 254 | 
