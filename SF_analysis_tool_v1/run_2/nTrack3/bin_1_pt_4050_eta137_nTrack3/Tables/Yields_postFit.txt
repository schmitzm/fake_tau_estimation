 |       | N_{jets}_M_1b | N_{jets}_nM_1b | 
 | #it{e#rightarrow#tau} | 8.27438 pm 0 | 17.9902 pm 0 | 
 | #it{h#rightarrow#tau} | 54.1454 pm 4.60133 | 187.5 pm 15.934 | 
 | #it{#mu#rightarrow#tau} | 18.2565 pm 0 | 25.1028 pm 0 | 
 | #it{#tau} | 76.9401 pm 0 | 23.4073 pm 0 | 
 | Total | 157.616 pm 4.60133 | 254.001 pm 15.934 | 
 | Data | 179 | 254 | 
