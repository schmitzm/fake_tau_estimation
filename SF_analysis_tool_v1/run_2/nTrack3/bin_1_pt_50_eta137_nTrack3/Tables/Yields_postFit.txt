 |       | N_{jets}_M_1b | N_{jets}_nM_1b | 
 | #it{e#rightarrow#tau} | 12.9626 pm 0 | 22.8724 pm 0 | 
 | #it{h#rightarrow#tau} | 54.9265 pm 5.62204 | 152.266 pm 15.5853 | 
 | #it{#mu#rightarrow#tau} | 14.2026 pm 0 | 18.9592 pm 0 | 
 | #it{#tau} | 184.804 pm 0 | 48.9027 pm 0 | 
 | Total | 266.895 pm 5.62204 | 243.001 pm 15.5853 | 
 | Data | 271 | 243 | 
