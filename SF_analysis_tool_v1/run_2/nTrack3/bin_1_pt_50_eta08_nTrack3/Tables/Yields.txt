 |       | N_{jets}_M_1b | N_{jets}_nM_1b | 
 | #it{e#rightarrow#tau} | 24.0619 pm 0 | 50.843 pm 0 | 
 | #it{h#rightarrow#tau} | 97.8137 pm 0 | 200.879 pm 0 | 
 | #it{#mu#rightarrow#tau} | 24.5727 pm 0 | 35.2673 pm 0 | 
 | #it{#tau} | 369.371 pm 0 | 99.5646 pm 0 | 
 | Total | 515.819 pm 0 | 386.554 pm 0 | 
 | Data | 484 | 423 | 
