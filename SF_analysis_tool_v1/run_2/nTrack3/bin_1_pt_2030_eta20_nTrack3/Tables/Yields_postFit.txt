 |       | N_{jets}_M_1b | N_{jets}_nM_1b | 
 | #it{e#rightarrow#tau} | 4.82234 pm 0 | 87.4353 pm 0 | 
 | #it{h#rightarrow#tau} | 262.181 pm 10.2335 | 838.406 pm 32.7248 | 
 | #it{#mu#rightarrow#tau} | 42.1238 pm 0 | 109.964 pm 0 | 
 | #it{#tau} | 51.3993 pm 0 | 35.1901 pm 0 | 
 | Total | 360.526 pm 10.2335 | 1071 pm 32.7248 | 
 | Data | 559 | 1071 | 
