 |       | N_{jets}_M_1b | N_{jets}_nM_1b | 
 | #it{e#rightarrow#tau} | 54.6236 pm 0 | 91.4873 pm 0 | 
 | #it{h#rightarrow#tau} | 610.232 pm 14.7709 | 1514.02 pm 36.6474 | 
 | #it{#mu#rightarrow#tau} | 73.7655 pm 0 | 126.481 pm 0 | 
 | #it{#tau} | 117.306 pm 0 | 67.8721 pm 0 | 
 | Total | 855.927 pm 14.7709 | 1799.86 pm 36.6474 | 
 | Data | 877 | 1782 | 
