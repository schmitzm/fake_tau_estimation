 |       | N_{jets}_M_1b | N_{jets}_nM_1b | 
 | #it{e#rightarrow#tau} | 55.9035 pm 0 | 35.5262 pm 0 | 
 | #it{h#rightarrow#tau} | 289.926 pm 21.9623 | 263.292 pm 19.9448 | 
 | #it{#mu#rightarrow#tau} | 24.8102 pm 0 | 19.5781 pm 0 | 
 | #it{#tau} | 508.051 pm 0 | 79.5419 pm 0 | 
 | Total | 878.691 pm 21.9623 | 397.939 pm 19.9448 | 
 | Data | 782 | 398 | 
