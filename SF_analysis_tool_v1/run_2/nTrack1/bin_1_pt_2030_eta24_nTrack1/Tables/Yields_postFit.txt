 |       | N_{jets}_M_1b | N_{jets}_nM_1b | 
 | #it{e#rightarrow#tau} | 50.9229 pm 0 | 58.4306 pm 0 | 
 | #it{h#rightarrow#tau} | 1857.14 pm 52.0036 | 1389.04 pm 38.8959 | 
 | #it{#mu#rightarrow#tau} | 37.7907 pm 0 | 33.314 pm 0 | 
 | #it{#tau} | 145.874 pm 0 | 32.193 pm 0 | 
 | Total | 2091.73 pm 52.0036 | 1512.98 pm 38.8959 | 
 | Data | 1959 | 1513 | 
