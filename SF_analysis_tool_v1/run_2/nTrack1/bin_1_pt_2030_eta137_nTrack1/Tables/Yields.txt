 |       | N_{jets}_M_1b | N_{jets}_nM_1b | 
 | #it{e#rightarrow#tau} | 119.389 pm 0 | 134.37 pm 0 | 
 | #it{h#rightarrow#tau} | 1862.85 pm 0 | 1376.11 pm 0 | 
 | #it{#mu#rightarrow#tau} | 109.455 pm 0 | 99.1431 pm 0 | 
 | #it{#tau} | 539.558 pm 0 | 108.361 pm 0 | 
 | Total | 2631.26 pm 0 | 1717.99 pm 0 | 
 | Data | 3228 | 2200 | 
