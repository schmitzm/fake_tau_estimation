 |       | N_{jets}_M_1b | N_{jets}_nM_1b | 
 | #it{e#rightarrow#tau} | 119.389 pm 0 | 134.37 pm 0 | 
 | #it{h#rightarrow#tau} | 2484.54 pm 42.3216 | 1835.36 pm 31.2634 | 
 | #it{#mu#rightarrow#tau} | 109.455 pm 0 | 99.1431 pm 0 | 
 | #it{#tau} | 539.558 pm 0 | 108.361 pm 0 | 
 | Total | 3252.95 pm 42.3216 | 2177.23 pm 31.2634 | 
 | Data | 3228 | 2200 | 
