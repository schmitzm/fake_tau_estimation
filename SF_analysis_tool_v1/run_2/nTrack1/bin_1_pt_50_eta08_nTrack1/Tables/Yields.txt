 |       | N_{jets}_M_1b | N_{jets}_nM_1b | 
 | #it{e#rightarrow#tau} | 60.9163 pm 0 | 45.6434 pm 0 | 
 | #it{h#rightarrow#tau} | 343.93 pm 0 | 256.748 pm 0 | 
 | #it{#mu#rightarrow#tau} | 42.7264 pm 0 | 29.8435 pm 0 | 
 | #it{#tau} | 989.933 pm 0 | 147.057 pm 0 | 
 | Total | 1437.51 pm 0 | 479.291 pm 0 | 
 | Data | 1440 | 582 | 
