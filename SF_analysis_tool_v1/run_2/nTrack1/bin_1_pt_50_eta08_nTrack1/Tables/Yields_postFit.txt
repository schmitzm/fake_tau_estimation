 |       | N_{jets}_M_1b | N_{jets}_nM_1b | 
 | #it{e#rightarrow#tau} | 60.9163 pm 0 | 45.6434 pm 0 | 
 | #it{h#rightarrow#tau} | 481.516 pm 32.3123 | 359.457 pm 24.1215 | 
 | #it{#mu#rightarrow#tau} | 42.7264 pm 0 | 29.8435 pm 0 | 
 | #it{#tau} | 989.933 pm 0 | 147.057 pm 0 | 
 | Total | 1575.09 pm 32.3123 | 582 pm 24.1215 | 
 | Data | 1440 | 582 | 
