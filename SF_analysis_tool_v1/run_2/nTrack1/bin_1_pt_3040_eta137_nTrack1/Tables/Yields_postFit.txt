 |       | N_{jets}_M_1b | N_{jets}_nM_1b | 
 | #it{e#rightarrow#tau} | 58.35 pm 0 | 56.2105 pm 0 | 
 | #it{h#rightarrow#tau} | 777.892 pm 35.988 | 587.156 pm 27.1639 | 
 | #it{#mu#rightarrow#tau} | 60.5959 pm 0 | 31.0171 pm 0 | 
 | #it{#tau} | 384.184 pm 0 | 63.5962 pm 0 | 
 | Total | 1281.02 pm 35.988 | 737.98 pm 27.1639 | 
 | Data | 1203 | 738 | 
