#!/bin/bash

#-------------------
#to give permission
#chmod +x run.sh

#------------------------------------------------------
#name of config file for the fitting and SF calculation
#it is imperative that the analysis_config.sh file is in the folder part_2 and the run.sh script one above
source part_2/analysis_config.sh

#---------------------------------
#check whether to create the lists

if [ "$create_lists" = "yes" ]
then
echo "create lists"
python3 part_2/create_list_of_SF.py
fi

#---------------------------------
#check whether to do plots

if [ "$do_plots" = "yes" ]
then
echo "do plots"
./part_2/SF_plotter.py
fi

#not ready yet
#---------------------------------
#check whether to apply the SFs

if [ "$apply_SF" = "yes" ]
then
echo "apply SF"
./part_2/JFTCorrector.py
fi

#-----------------------
#create folder structure

mkdir "$folder_name"
mv part_2/table*.csv "$folder_name"
mv part_2/*.pdf "$folder_name"

echo '------------------------'
echo '         DONE           '
echo '------------------------'
