
This is a README file for the analysis of fakes in the 1 hadronic tau and two light lepton channel tool (lephad)
================================================================================================================


Structure of the tool:
----------------------
The tool consists of two parts: 1) the TRExFitter fitting part
                                2) the extraction of the fitting information, plotting and applying the normfactors to the whole region part

In Detail it consists of:
  Part 1):
  - a set of TRExFitter config files in a given folder Structure (called "SF_fit_TRF_config_files") and a fixed binning of eta, pt and number of prongs
      (a change in these requires changes in those config files and additional files)
  - a bash script to execute those config files and create and saving the results in a specific folder Structure (called "fit_all.sh")
  - a config file that defines: (called "fitting_config.sh")
    - the path for the bash script to find the TRExFitter config files
    - chooses the name of the folder in which the results are saved
    
  Part 2): 
  - a python script that reads from the folder in the structure of the produced fits and saves the Correction factors in a csv file
      (called "create_list_of_SF.py")
  - a python plot script that plots the dependence of the Correctionfactors in a binning of eta, pt and number of prongs
      (called "SF_plotter.py")
  - a python script that applies the normfactors to the samples for the 'postfit' plots
      (called "JFTCorrector.py") ----> still work in progress 
  - a config file to set the parameters for the three python scripts (called "analysis_config.sh")
  - a script to run the tool (callled "run.sh")


How to run the tool:
--------------------

To run part 1): 
---------------                          
Make sure to have executed in the TRExFitter folder:
    
    setupATLAS
    source setup.sh

Then make sure the following is in the TRExFitter folder:
  - the "fitting_config.sh" file
  - the "fit_all.sh" bash script
  - the "SF_fit_TRF_config_files" folder

Make sure the "fit_all.sh" script has been given permission:
chmod +x fit_all.sh
Then set the parameters in the "fitting_config.sh". Those being:
  - name of the folder in which all the fit info is saved eg: run_1 (in line 12)
  - name of the path to the TRExFitter config files for 1-prong (either relative or absolute path)  eg: SF_fit_TRF_config_files/1prong (in line 17)
  - name of the path to the TRExFitter config files for 1-prong (either relative or absolute path)  eg: SF_fit_TRF_config_files/1prong (in line 23)
  - Those config files are in the folderstructure: part_1/SF_fit_TRF_config_files/nTrack1
                                                   part_1/SF_fit_TRF_config_files/nTrack3

If all the above is the case (should be the standard case in the config if) run: 

    ./fit_all.sh

The output should be a folder with name: "folder_name" eg. "run_1" and subfolders nTrack1, nTrack3 each containing the fit data


Before running part 2) set up the environment for the required packages:
------------------------------------------------------------------------
You can set up your own environment or use the recommended here. The main packages used are:
- root, root_numpy, numpy, pandas, csv, glob, array, os

If you want to use the given environment you can do the following.
Using the requirements.txt:

    conda create --name SF_analysis --file requirements.txt

Or you can use the .yml file:

    conda env create --name SF_analysis --file=SF_analysis.yml


To run part 2):
---------------
Make sure the "folder_name" (eg. "run_1") is or has been copied to the same environment (eg. the "SF_analysis_tool_v1" folder) as well as:

In the part_2 folder:
  - the "create_list_of_SF.py" script
  - the "paths_1prong.txt" file
  - the "paths_3prong.txt" file
  - the "analysis_config.sh" file
  - the "SF_plotter.py" script
  - the "JTFCorrector.py" script (work in progress)

**----> It is recommended to use as the aforementioned environement in which to run part 2) the "SF_analysis_tool_v1" folder.**
      **Since the standard config file's paths are defined relative to there.**
  
Before running do each once to give the required permission rights:

    chmod +x part_2/SF_plotter.py
    chmod +x run.sh
    chmod +x part_2/JFTCorrector.py

Since they were written as bash scripts.

To run the analysis set the required configuration in "analysis_config.sh" and set the paths in the "paths_1prong.txt" files. See below.
                                                                                                    "paths_3prong.txt"

**----> Run with:**

    ./run.sh

It is useful to note that: 
  - the name of the folder is set in the config as well
  - that permission rights have to be given once for each .sh or .py file in part 2

**----> When doing python plots it is necessary to run the create lists option as well**
**----> When doing JFTCorrection it is necessary to run the create lists option as well**

Make sure to delete, move or rename the "folder_name" folder for each run otherwise it might get overwritten.

**----> The JFTCorrection is only possible to do in baf. The required path to the samples which get modified can be given via the option**
      **in "analysis_config.sh". It is advised to be careful with this option!**

The output of option 1 and 2 can be found in the folder with given name.


Explanation to the **JFTCorrection.py**:
----------------------------------------
This script should be run next to the "table_nTrack1/3.csv" files. Which means after running the create lists option (option 1).
A path to the samples with the syntax: 
    
    "/path/path/sample_folder/*.root"

Should be given to "analysis_config.sh" which should be in the same fodler as the "JFTCorrection.py" script.

What the script does is it adds a new branch called "weight_JFTCorr" to the samples. If one now wants to plot or fit now with SF corrected
one has to change the **MCweight: "weight_nominal"** in the Job section of the given TRExFitter config to:

    MCweight: "weight_JFTcorr"


About the "paths_1prong.txt" and "paths_3prong.txt" files:
----------------------------------------------------------
They are necessary to create the SF list. In them set the paths (either relative or absolute) to the fit data. In the default version of
this tool they reference ton run_2 with the SF from my run_2 bin_1 fit run. This is also why this folder is included in this tool and
can be deleted if necessary.

**----> Be Beware to set the paths correctly then in those files though.**

**----> Also do NOT add empty lines or comments in these files**


About the TRExFitter Config files:
----------------------------------
The Config files are found in: part_1/SF_fit_TRF_config_files/. This folder is subdivided into: nTrack1 in which
                                                                                                nTrack3
the config files for TRExFitter can be found. For details in writing the TRExFitter config files
please check: <https://gitlab.cern.ch/TRExStats/TRExFitter/-/tree/master>

Please note: 
  - to change the binning additional files need to be created and the Selection criteria changed
  - these config files run on samples specified in the Job section of the file and caution needs to be applied if they should be used
  - the nomenclature is: "bin_1_pt"number from to"_eta"highest eta boundary" for example:
    bin_1_pt2030_eta08 means the config file for: 20 =< pt < 30 and eta <= 0.8
  - and for eta we have: 08 --> 0.8, 137 --> 1.37, 20 --> 2.0, 24 --> 2.4


FAQ:
----
Why does part 1 not work?
-  Part 1 needs to be executed in the TRExFitter main folder. The paths to the config files need to be correct.
   The recommended procedure is: - copy from part 1 the whole content from part 1 into TRExFitter (preferably the main folder),
   don't forget setupATLAS and source setup.sh and to give permission to the fit_all.sh script. And then run it.

Why does part 2 not work?
- One reason could be a wrong implementation of the "paths_1/3prong.txt" files
- Make sure the folder from part 1 is there and can be found. 

Where are the config files?
- They can be found in respectively part_1 and part_2 and are called: fitting_config.sh (part 1)
                                                                      analysis_config.sh (part 2)

Where do I put the output folder from part 1 for part 2?
- Directly in the folder of the tool (SF_analysis_tool_v1) 

Why v1 in the name?
- Depending on the requirement of the analysis different config files and features (for example a log for the fits in part 1) may be created.

Why does it not work on baf yet?
- Looking in to it. It is related to the way python3 and root are used in baf.

Contact:
--------
Written by Marvin Schmitz.
Uploaded to: 
E-Mail: <marvin.schmitz@protonmail.com>





