#!/bin/bash

#-------------------
#to give permission
#chmod +x fit_all.sh

#------------------
#source config file
#it is imperative that the fitting_config.sh file is in the same folder as the fit_all.sh script
source fitting_config.sh

#-----------------------------------------------------------------------------------------------------------------
#defines the number of files in the path (doing it this way assumes same number of config files for 1 and 3 prong)
#if that number is different --> define: n2=$(ls -1 "$path_3prong" | wc -l) for example

n=$(ls -1 "$path_1prong" | wc -l)
x=1

#----------------------------------------------------
#execute the TRExFitter cmd for all files in the path

while [ $x -le $n ]
do
  #echo "$(ls -1 | sed -n "$x p")"
  trex-fitter nwdfp /cephfs/user/s6mvschm/TRExFitter/config/bin_1/uncertainties_truthtau/minus/nTrack1/"$(ls -1 "$path_1prong" | sed -n "$x p")"
  trex-fitter nwdfp /cephfs/user/s6mvschm/TRExFitter/config/bin_1/uncertainties_truthtau/minus/nTrack3/"$(ls -1 "$path_3prong" | sed -n "$x p")"
  x=$(( $x + 1 ))
done

#-----------------------
#create folder structure

mkdir $folder_name
mkdir $folder_name/nTrack1
mkdir $folder_name/nTrack3
mv bin* $folder_name
mv $folder_name/*nTrack1 $folder_name/nTrack1
mv $folder_name/*nTrack3 $folder_name/nTrack3

echo '-----------------------------------'
echo '-----------------------------------'
echo '              DONE                 '
echo '-----------------------------------'
echo '-----------------------------------'