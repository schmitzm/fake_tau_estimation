#This is the config file for the TRExFitter SF fit bash script
#it can be sourced using:

#source fitting_config.sh

#Be careful to respect the right paths in the script
#For more information see the README for the SF calculation tool


#-------------------------------------------------------
#Name of the folder in which to save the Fitinformation:

folder_name="run_1"


#-----------------------------------------------------
#Path of the TRExFitter config files for 1-prong fits:

path_1prong="SF_fit_TRF_config_files/nTrack1"


#-----------------------------------------------------
#Path of the TRExFitter config files for 1-prong fits:

path_3prong="SF_fit_TRF_config_files/nTrack3"
