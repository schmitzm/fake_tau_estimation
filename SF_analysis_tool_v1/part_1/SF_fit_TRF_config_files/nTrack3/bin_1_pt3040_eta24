% --------------- %
% ---  JOB    --- %
% --------------- %

  Job: "bin_1_pt_3040_eta24_nTrack3"
  CmeLabel: "13 TeV"
  POI: "norm_tau"
  ReadFrom: NTUP
  NtuplePaths: "/cephfs/user/s6mvschm/tHq/run/condor/v31_lephad_Loose/nominal"
  Label: "#tau_{lep}#tau_{had}"
  LumiLabel: "140.5 fb^{-1}"
  MCweight: "weight_nominal"
  Lumi: 139
  %Selection: "isTight_lep1 && isTight_lep2 && isTight_lep3"
  PlotOptions: "LEFT,NOXERR,NOENDERR"
  NtupleName: "tHqLoop_nominal_Loose"
  DebugLevel: 1
  MCstatThreshold: none
  %MCstatConstraint: "Poisson"
  %SystControlPlots: FALSE
  %SystPruningShape: 0.01
  %SystPruningNorm: 0.01
  CorrelationThreshold: 0.20
  HistoChecks: NOCRASH
  SplitHistoFiles: TRUE
  ImageFormat: "png"
  SystCategoryTables: TRUE
  RankingPlot: "all"
  RankingMaxNP: 10
  DoSummaryPlot: TRUE
  DoTables: TRUE
  DoSignalRegionsPlot: TRUE
  DoPieChartPlot: TRUE
  GuessMCStatEmptyBins: TRUE
  %BlindingThreshold: 0.05
    
% --------------- %
% ---  FIT    --- %
% --------------- %

Fit: "fit"
  FitType: SPLUSB
  FitRegion: CRONLY
  %POIAsimov: 1
  UseMinos: TRUE
  %StatOnly: TRUE

% --------------- %
% ---  LIMIT  --- %
% --------------- %

Limit: "limit"
  LimitType: ASYMPTOTIC
 % POIAsimov: 1

% --------------- %
% --- REGIONS --- %
% --------------- %


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% non-Medium tau ID = Control Region
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Region: "SR_nJets_1b_M"
  Type: SIGNAL
  Variable: "m_njets",1,1,7
  VariableTitle: "N_{jets}"
  Label: "N_{jets}_M_1b"
  Selection: "m_nbjets==1 && had_tau_1_tight && had_tau_1_nTrack==3 && had_tau_1_pt >= 30 && had_tau_1_pt < 40 && abs(had_tau_1_eta) >= 2.0 && abs(had_tau_1_eta) < 2.5"


Region: "CR_nJets_1b_nM"
  Type: CONTROL
  Variable: "m_njets",1,1,7
  VariableTitle: "N_{jets}"
  Label: "N_{jets}_nM_1b"
  Selection: "m_nbjets==1 && !had_tau_1_tight && had_tau_1_nTrack==3 && had_tau_1_pt >= 30 && had_tau_1_pt < 40 && abs(had_tau_1_eta) >= 2.0 && abs(had_tau_1_eta) < 2.5"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Normal samples  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Sample: "Data"
  Type: data
  Title: "Data"
  NtupleFiles: data15_13TeV,data16_13TeV,data17_13TeV,data18_13TeV

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Specified samples for fit %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Sample: "el"
  Type: BACKGROUND
  Title: "#it{e#rightarrow#tau}"
  FillColor: 8
  LineColor: 1
  Selection: "abs(had_tau_true_pdg)==11"
  NtupleFiles: mc16*


Sample: "had"
  Type: BACKGROUND
  Title: "#it{h#rightarrow#tau}"
  FillColor: 2
  LineColor: 1
  Selection: "had_tau_true_pdg==0"
  NtupleFiles: mc16*


Sample: "mu"
 Type: BACKGROUND
  Title: "#it{#mu#rightarrow#tau}"
  FillColor: 9
  LineColor: 1
  Selection: "abs(had_tau_true_pdg)==13"
  NtupleFiles: mc16*

Sample: "tau"
  Type: SIGNAL
  Title: "#it{#tau}"
  FillColor: 7
  LineColor: 2
  Selection: "abs(had_tau_true_pdg)==15"
  NtupleFiles: mc16*

% --------------- %
% - NORMFACTORS - %
% --------------- %

NormFactor: "norm_mu"
  Title: "#it{#mu}(#it{#tau})"
  Nominal: 1.0
  Min: 1
  Max: 1
  Samples: mu

NormFactor: "norm_el"
  Title: "#it{el}(#it{#tau})"
  Nominal: 1.0
  Min: 1
  Max: 1
  Samples: el

NormFactor: "norm_had"
  Title: "#it{had}(#it{#tau})"
  Nominal: 1.0
  Min: 0.1
  Max: 10.0
  Samples: had
  
NormFactor: "norm_tau"
  Title: "#it{#tau}"
  Nominal: 1.0
  Min: 1
  Max: 1
  Samples: tau

