#!/usr/bin/env python

import numpy as np
import pandas as pd
import array
import ROOT
import root_numpy as rootnp
import glob

#opens analysis_config as source
config = glob.glob('**/analysis_config.sh', recursive=True)[0]
exec(open(config).read())

#------------------------------------------------------------------------
#this part defines functions to read from a csv file (in a certain order)
#------------------------------------------------------------------------

def read_file(path):
    # open file in read mode
    with open(path, 'r') as fou:
        lines = fou.readlines()
    fou.close()

    return lines

#list contains a list with each entry being a line in the csv file
def sort(list):
    #split list entries in to list of words like
    #['bin_1_pt_2030_eta06_nTrack1.txt', '1.0', '0.0', '-0.0', '1.35276', '0.0311975', '-0.0311975', '1.0', '0.0', '-0.0', '1.0', '0.0', '-0.0\n']
    
    #create a list for every eta binning
    #and its errors
    eta08 = []
    err08 = []

    eta137 = []
    err137 = []

    eta20 = []
    err20 = []

    eta24 = []
    err24 = []
    
    #tells the loop which entry is which eta
    #needs to be changed if BINNING CHANGES OR CSV FILE LOOKS DIFFERENT!
    iterate_08 = [1,5,9,13]
    iterate_137 = [2,6,10,14]
    iterate_20 = [3,7,11,15]
    iterate_24 = [4,8,12,16]

    #assign eta bins with pt entries + stat errors
    for a in iterate_08:
        eta08.append(float(list[a].split("\t")[4]))
        err08.append(float(list[a].split("\t")[5]))

    for a in iterate_137:
        eta137.append(float(list[a].split("\t")[4]))
        err137.append(float(list[a].split("\t")[5]))

    for a in iterate_20:
        eta20.append(float(list[a].split("\t")[4]))
        err20.append(float(list[a].split("\t")[5]))

    for a in iterate_24:
        eta24.append(float(list[a].split("\t")[4]))
        err24.append(float(list[a].split("\t")[5]))

    #lama is dictionary with sorted list entries
    lama = {
        "eta08": eta08,
        "err08": err08,
        "eta137": eta137,
        "err137": err137,
        "eta20": eta20,
        "err20": err20,
        "eta24": eta24,
        "err24": err24,
        }

    return lama


#-------------------------------------------------------------------------------------------------------
#this part defines functions to apply the Corrfactor to samples in a given path (defined in config file)
#-------------------------------------------------------------------------------------------------------


# Class of different styles
class style():
    BLACK = '\033[30m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    MAGENTA = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[37m'
    UNDERLINE = '\033[4m'
    RESET = '\033[0m'

#lama_1, lama_3 are dictionaries with sorted list entries given later by sort(list)
def getCorFactor(row, lama_1, lama_3):
    if (row['had_tau_1_nTrack']==1):
        if ( abs(row['had_tau_1_eta']) <= 0.8): 
            if ( row['had_tau_1_pt'] < 30 ): return lama_1['eta08'][0]
            elif ( row['had_tau_1_pt'] < 40 ): return lama_1['eta08'][1]
            elif ( row['had_tau_1_pt'] < 50 ): return lama_1['eta08'][2]
            else: return lama_1['eta08'][3]
        elif ( abs(row['had_tau_1_eta']) <= 1.37): 
            if ( row['had_tau_1_pt'] < 30 ): return lama_1['eta137'][0]
            elif ( row['had_tau_1_pt'] < 40 ): return lama_1['eta137'][1]
            elif ( row['had_tau_1_pt'] < 50 ): return lama_1['eta137'][2]
            else: return lama_1['eta137'][3]
        elif ( abs(row['had_tau_1_eta']) < 1.52): 
            return 1.0
        elif ( abs(row['had_tau_1_eta']) < 2.0): 
            if ( row['had_tau_1_pt'] < 30 ): return lama_1['eta20'][0]
            elif ( row['had_tau_1_pt'] < 40 ): return lama_1['eta20'][1]
            elif ( row['had_tau_1_pt'] < 50 ): return lama_1['eta20'][2]
            else: return lama_1['eta20'][3]
        elif ( abs(row['had_tau_1_eta']) < 2.5): 
            if ( row['had_tau_1_pt'] < 30 ): return lama_1['eta24'][0]
            elif ( row['had_tau_1_pt'] < 40 ): return lama_1['eta24'][1]
            elif ( row['had_tau_1_pt'] < 50 ): return lama_1['eta24'][2]
            else: return lama_1['eta24'][3]

    elif (row['had_tau_1_nTrack']==3):
        if ( abs(row['had_tau_1_eta']) <= 0.8): 
            if ( row['had_tau_1_pt'] < 30 ): return lama_3['eta08'][0]
            elif ( row['had_tau_1_pt'] < 40 ): return lama_3['eta08'][1]
            elif ( row['had_tau_1_pt'] < 50 ): return lama_3['eta08'][2]
            else: return lama_3['eta08'][3]
        elif ( abs(row['had_tau_1_eta']) <= 1.37): 
            if ( row['had_tau_1_pt'] < 30 ): return lama_3['eta137'][0]
            elif ( row['had_tau_1_pt'] < 40 ): return lama_3['eta137'][1]
            elif ( row['had_tau_1_pt'] < 50 ): return lama_3['eta137'][2]
            else: return lama_3['eta137'][3]
        elif ( abs(row['had_tau_1_eta']) < 1.52): 
            return 1.0
        elif ( abs(row['had_tau_1_eta']) < 2.0): 
            if ( row['had_tau_1_pt'] < 30 ): return lama_3['eta20'][0]
            elif ( row['had_tau_1_pt'] < 40 ): return lama_3['eta20'][1]
            elif ( row['had_tau_1_pt'] < 50 ): return lama_3['eta20'][2]
            else: return lama_3['eta20'][3]
        elif ( abs(row['had_tau_1_eta']) < 2.5): 
            if ( row['had_tau_1_pt'] < 30 ): return lama_3['eta24'][0]
            elif ( row['had_tau_1_pt'] < 40 ): return lama_3['eta24'][1]
            elif ( row['had_tau_1_pt'] < 50 ): return lama_3['eta24'][2]
            else: return lama_3['eta24'][3]
    return 1.0


def main():
    
    #dictionary for 1prong
    table_1 = glob.glob('**/table_nTrack1.csv', recursive=True)[0]
    lines_1prong = read_file(table_1)
    lama_1prong = sort(lines_1prong)

    #dictionary for 3prong
    table_2 = glob.glob('**/table_nTrack3.csv', recursive=True)[0]
    lines_3prong = read_file(table_2)
    lama_3prong = sort(lines_3prong)

    print(style.YELLOW+"Reading ntuples..."+style.RESET)
    ntuplefiles = []
    
    #JFTpath is the path from the config file ('analysis_config.sh')
    for file in glob.glob(JFTpath):
        ntuplefiles.append(file)

    for file in ntuplefiles:
        print(file)
        #Get data from each file
        f = ROOT.TFile(file)
        tree = f.Get('tHqLoop_nominal_Loose')
        vars_all =['had_tau_1_pt',
                    'had_tau_1_eta',
                    'had_tau_1_nTrack',
                    'had_tau_true_pdg', 
                    'weight_nominal']

        #converts root tree to an array
        data = rootnp.tree2array(tree, branches=list(vars_all), selection="", start=0, stop=None)
        #creates a panda tabular from that array 
        dframe = pd.DataFrame(data, columns=vars_all, dtype=np.float32)


        if dframe.shape[0]:
            dframe['weight_JFTcorr'] = dframe.apply(lambda row: getCorFactor(row, lama_1prong, lama_3prong)*row['weight_nominal'] if row['had_tau_true_pdg']==0 else row['weight_nominal'], axis=1)
            #weight_JFTcorr = np.ravel(dframe['weight_JFTcorr'])
            weight_JFTcorr = np.array( dframe['weight_JFTcorr'], 
                                        dtype=[('weight_JFTcorr',np.float32)])
            #print(weight_JFTcorr.dtype)
            #weight_corr.dtype = [('weight_corr', np.float32)]

            #applies to sample file
            rootnp.array2root(weight_JFTcorr, file, "tHqLoop_nominal_Loose", mode='update')
        else:
            print('The following file is empty: ', file)


if __name__ == '__main__':
    main()