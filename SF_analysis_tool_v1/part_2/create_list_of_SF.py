#!/usr/bin/env python

import csv
import glob

############################################################################################################################
## a file called 'paths.txt' has to be in the same folder as the program containing the paths for the Corr.Factors each line
############################################################################################################################

#reads the Correctfactors from fit with path being full path and return list with (name, CorrFactors)
def read_file(path):

    #open file
    with open(path) as fou:
        lines = fou.readlines()
    fou.close()

    #extract relevant informtion from list lines into strings
    str_el = lines[1]
    str_had = lines[2]
    str_mu = lines[3]
    str_tau = lines[4]

    #extract number from strings
    el = str_el.split(" ")
    num_el = float(el[2])
    l_unc_el = float(el[3])
    r_unc_el = float(el[4])
 
    had = str_had.split(" ")
    num_had = float(had[2])
    l_unc_had = float(had[3])
    r_unc_had = float(had[4])

    mu = str_mu.split(" ")
    num_mu = float(mu[2])
    l_unc_mu = float(mu[3])
    r_unc_mu = float(mu[4])

    tau = str_tau.split(" ")  
    num_tau = float(tau[2])
    l_unc_tau = float(tau[3])
    r_unc_tau = float(tau[4])

    #call name the last string entry in the list of the path of str path
    name_list = path.split("/")
    name_length = len(name_list)
    name = name_list[name_length-1]

    #return list with Correctionfactors
    return[name, num_el, l_unc_el, r_unc_el, num_had, l_unc_had, r_unc_had, num_mu, l_unc_mu, r_unc_mu, num_tau, l_unc_tau, r_unc_tau]

#list with lists of correctionfactors for all the files: so lenght of list is # of files read from
def write_table(list, path):
    with open(path, 'wt') as csvfile:
        writer = csv.writer(csvfile, delimiter='\t', lineterminator='\n')
        # Add the header row
        writer.writerow(['eta_pt_nTrack file','el', '+D_el', '-D_el', 'had', '+D_had', '-D_had', 'mu', '+D_mu', '-D_mu', 'tau', '+D_tau', '-D_tau'])

        for i in range(0,len(list)):
            # Add the data row
            writer.writerow(list[i])
            
    csvfile.close()

#create a list of lists which each entry being [name_of_eta_pt_nTrack, el, had, mu, tau] for each eta,pt,prong value
#list_of_paths = 'all paths for analysis --> enter manually'
def create_list(list_of_paths):
    og_list = []*len(list_of_paths)
    for i in range(0,len(list_of_paths)):
        og_list.append(read_file(list_of_paths[i]))
    return og_list


def main():
    
    #list_of_paths read from 'paths_1prong.txt' and --> put in l_paths
    path_1 = glob.glob('*/paths_1prong.txt', recursive=True)[0]
    with open(path_1) as f1:
        #done this way since this strips the \n part for each line as well
        l_paths1 = [line.rstrip() for line in f1]
    f1.close()

    path_2 = glob.glob('*/paths_3prong.txt', recursive=True)[0]
    with open(path_2) as f2:
        #done this way since this strips the \n part for each line as well
        l_paths2 = [line.rstrip() for line in f2]
    f2.close()

    #create_list og_list with [name_of_eta_pt_nTrack, el, had, mu, tau] for each path
    #kuh is the og_list
    kuh = create_list(l_paths1)
    muh = create_list(l_paths2)
    
    #writes table into 'table_nTrack1.csv'
    table_1 = glob.glob('**/table_nTrack1.csv', recursive=True)[0]
    table_2 = glob.glob('**/table_nTrack3.csv', recursive=True)[0]
    write_table(kuh, table_1)
    write_table(muh, table_2)



if __name__ == '__main__':
    main()