#This is the config file for the run.sh bash script
#and the JFTCorrector.py script
#it can be sourced using:

#source analysis_config.sh
#exec(open('analysis_config.sh').read())

#Be careful to respect the right paths in the scripts
#For more information see the README for the SF calculation tool


#--------------------------------------------------
#Careful: the config is "case sensitive"

#-----------------------------------------------
#1) Create tables with SF's (Do create_list.py):
create_lists='yes'

#-----------------------------------
#2) Create plots (Do SF_plotter.py):
do_plots='yes'

#---------------------------------------------------------------------
#3) Apply SF to samples (Do JTFCorrector.py): (still work in progress)
apply_SF='no'


#-------------------------------------------------------
#Name of the folder in which to save the Fitinformation:
folder_name='analysis_run_2'


#------------------------------------------------
#Path to the samples for JFTCorrection (option 3)
#ONLY change the part before the *.root
JFTpath='/home/marvin/master_thesis/los_alamos/manhattan_samples/*.root'
#JFTpath='/Users/okiverny/workspace/tH/v31_lephad_Loose/nominal_v1/*.root'
