#!/usr/bin/env python

import os
import ROOT
from ROOT import gROOT,TGraphErrors,TMultiGraph,TGraph
from array import array
import csv
import glob

ROOT.gStyle.SetOptStat(0)


#------------------------------------------------------------------------
#this part defines functions to read from a csv file (in a certain order)
#------------------------------------------------------------------------

def read_file(path):
    # open file in read mode
    with open(path, 'r') as fou:
        lines = fou.readlines()
    fou.close()

    return lines

#list contains a list with each entry being a line in the csv file
def sort(list):
    #split list entries in to list of words like
    #['bin_1_pt_2030_eta06_nTrack1.txt', '1.0', '0.0', '-0.0', '1.35276', '0.0311975', '-0.0311975', '1.0', '0.0', '-0.0', '1.0', '0.0', '-0.0\n']
    
    #create a list for every eta binning
    #and its errors
    eta08 = []
    err08 = []

    eta137 = []
    err137 = []

    eta20 = []
    err20 = []

    eta24 = []
    err24 = []
    
    #tells the loop which entry is which eta
    #needs to be changed if BINNING CHANGES OR CSV FILE LOOKS DIFFERENT!
    iterate_08 = [1,5,9,13]
    iterate_137 = [2,6,10,14]
    iterate_20 = [3,7,11,15]
    iterate_24 = [4,8,12,16]

    #assign eta bins with pt entries + stat errors
    for a in iterate_08:
        eta08.append(float(list[a].split("\t")[4]))
        err08.append(float(list[a].split("\t")[5]))

    for a in iterate_137:
        eta137.append(float(list[a].split("\t")[4]))
        err137.append(float(list[a].split("\t")[5]))

    for a in iterate_20:
        eta20.append(float(list[a].split("\t")[4]))
        err20.append(float(list[a].split("\t")[5]))

    for a in iterate_24:
        eta24.append(float(list[a].split("\t")[4]))
        err24.append(float(list[a].split("\t")[5]))

    #lama is dictionary with sorted list entries
    lama = {
        "eta08": eta08,
        "err08": err08,
        "eta137": eta137,
        "err137": err137,
        "eta20": eta20,
        "err20": err20,
        "eta24": eta24,
        "err24": err24,
        }

    return lama


#--------------------------------------------------------------------------------
#this part defines functions to actually plot and use the functions defined above
#--------------------------------------------------------------------------------

#lama is a dictionary with eta sorted pt values + errors
def plot_pt_eta_1_prong(lama):
    # define pt bins (even though they are called eta bins)
    n = 4 # number of bins (including empty bin)
    etamin = [20,30,40,50]
    etamax = [30,40,50,100]

    # compute bin centers
    etac, etaerr = [], []
    for i in range(len(etamin)):
        etac.append( (etamin[i] + etamax[i])/2 )
        etaerr.append( (etamax[i] - etamin[i])/2 )

    x  = array( 'f', etac )
    ex = array( 'f', etaerr )

    #######################
    # Define values 1-prong
    #######################
    # Define eta=0-0.8
    y1 = array( 'f', [lama['eta08'][0], lama['eta08'][1], lama['eta08'][2], lama['eta08'][3]] )
    ey1 = array( 'f', [lama['err08'][0], lama['err08'][1], lama['err08'][2], lama['err08'][3]] ) # stat errors

    # Define eta=0.8-1.37
    y2 = array( 'f', [lama['eta137'][0], lama['eta137'][1], lama['eta137'][2], lama['eta137'][3]] )
    ey2 = array( 'f', [lama['err137'][0], lama['err137'][1], lama['err137'][2], lama['err137'][3]] ) # stat errors

    # Define eta=1.52-2.0
    y3 = array( 'f', [lama['eta20'][0], lama['eta20'][1], lama['eta20'][2], lama['eta20'][3]] )
    ey3 = array( 'f', [lama['err20'][0], lama['err20'][1], lama['err20'][2], lama['err20'][3]] ) # stat errors

    # Define eta=2.1-2.4
    y4 = array( 'f', [lama['eta24'][0], lama['eta24'][1], lama['eta24'][2], lama['eta24'][3]] )
    ey4 = array( 'f', [lama['err24'][0], lama['err24'][1], lama['err24'][2], lama['err24'][3]] ) # stat errors

    #### Plotting preparation
    c = ROOT.TCanvas()
    c.SetGrid()
    c.GetFrame().SetFillColor( 21 )
    c.GetFrame().SetBorderSize( 12 )
    mg = TMultiGraph()
    mg.GetYaxis().SetTitle("Jet faking tau NormFactor")
    mg.GetXaxis().SetTitle("p_{T}^{#tau} [GeV]")

    #########################
    # First Graph for 1-prong
    #########################
    # for eta=0-0.8
    gr1 = TGraphErrors( n, x, y1, ex,ey1 )
    gr1.SetMarkerColor( 4 ), gr1.SetMarkerStyle( 21 ), gr1.SetLineColor( 1 ), gr1.SetLineWidth( 2 )
    mg.Add(gr1)
    
    # for eta=0.6-1.37
    gr2 = TGraphErrors( n, x, y2, ex,ey2 )
    gr2.SetMarkerColor( 2 ), gr2.SetMarkerStyle( 21 ), gr2.SetLineColor( 1 ), gr2.SetLineWidth( 2 )
    mg.Add(gr2)

    # for eta=1.52-2.0
    gr3 = TGraphErrors( n, x, y3, ex,ey3 )
    gr3.SetMarkerColor( 3 ), gr3.SetMarkerStyle( 21 ), gr3.SetLineColor( 1 ), gr3.SetLineWidth( 2 )
    mg.Add(gr3)

    # for eta=2.01-2.4
    gr4 = TGraphErrors( n, x, y4, ex,ey4 )
    gr4.SetMarkerColor( 6 ), gr4.SetMarkerStyle( 21 ), gr4.SetLineColor( 1 ), gr4.SetLineWidth( 2 )
    mg.Add(gr4)

    # Drawing multigraph
    mg.Draw( 'AP' )
    mg.SetMaximum(1.8)
    mg.SetMinimum(0.6)
    mg.GetXaxis().SetLimits(20,100)

    # Legend
    leg = ROOT.TLegend(0.11,0.7,0.59,0.89)
    leg.SetBorderSize(0), leg.SetTextFont(42), leg.SetTextSize(0.04)
    leg.SetFillColor(0), leg.SetLineColor(0), leg.SetFillStyle(0)
    leg.AddEntry(gr1,'1-prong & |#eta_{#tau}|=0-0.8',"ep")
    leg.AddEntry(gr2,'1-prong & |#eta_{#tau}|=0.8-1.37',"ep")
    leg.AddEntry(gr3,'1-prong & |#eta_{#tau}|=1.52-2.0',"ep")
    leg.AddEntry(gr4,'1-prong & |#eta_{#tau}|=2.01-2.4',"ep")
    leg.Draw()

    c.Update()
    ROOT.gPad.Modified()
    c.SaveAs('pt_eta_1_prong.pdf')

def plot_pt_eta_3_prong(lama):
    # define pt bins (even though they are called eta bins)
    n = 4 # number of bins (including empty bin)
    etamin = [20,30,40,50]
    etamax = [30,40,50,100]

    # compute bin centers
    etac, etaerr = [], []
    for i in range(len(etamin)):
        etac.append( (etamin[i] + etamax[i])/2 )
        etaerr.append( (etamax[i] - etamin[i])/2 )

    x  = array( 'f', etac )
    ex = array( 'f', etaerr )

    #######################
    # Define values 3-prong
    #######################
    # Define eta=0-0.8
    y1 = array( 'f', [lama['eta08'][0], lama['eta08'][1], lama['eta08'][2], lama['eta08'][3]] )
    ey1 = array( 'f', [lama['err08'][0], lama['err08'][1], lama['err08'][2], lama['err08'][3]] ) # stat errors

    # Define eta=0.8-1.37
    y2 = array( 'f', [lama['eta137'][0], lama['eta137'][1], lama['eta137'][2], lama['eta137'][3]] )
    ey2 = array( 'f', [lama['err137'][0], lama['err137'][1], lama['err137'][2], lama['err137'][3]] ) # stat errors

    # Define eta=1.52-2.0
    y3 = array( 'f', [lama['eta20'][0], lama['eta20'][1], lama['eta20'][2], lama['eta20'][3]] )
    ey3 = array( 'f', [lama['err20'][0], lama['err20'][1], lama['err20'][2], lama['err20'][3]] ) # stat errors

    # Define eta=2.1-2.4
    y4 = array( 'f', [lama['eta24'][0], lama['eta24'][1], lama['eta24'][2], lama['eta24'][3]] )
    ey4 = array( 'f', [lama['err24'][0], lama['err24'][1], lama['err24'][2], lama['err24'][3]] ) # stat errors

    #### Plotting preparation
    c = ROOT.TCanvas()
    c.SetGrid()
    c.GetFrame().SetFillColor( 21 )
    c.GetFrame().SetBorderSize( 12 )
    mg = TMultiGraph()
    mg.GetYaxis().SetTitle("Jet faking tau NormFactor")
    mg.GetXaxis().SetTitle("p_{T}^{#tau} [GeV]")

    #########################
    # First Graph for 1-prong
    #########################
    # for eta=0-0.8
    gr1 = TGraphErrors( n, x, y1, ex,ey1 )
    gr1.SetMarkerColor( 4 ), gr1.SetMarkerStyle( 21 ), gr1.SetLineColor( 1 ), gr1.SetLineWidth( 2 )
    mg.Add(gr1)
    
    # for eta=0.6-1.37
    gr2 = TGraphErrors( n, x, y2, ex,ey2 )
    gr2.SetMarkerColor( 2 ), gr2.SetMarkerStyle( 21 ), gr2.SetLineColor( 1 ), gr2.SetLineWidth( 2 )
    mg.Add(gr2)

    # for eta=1.52-2.0
    gr3 = TGraphErrors( n, x, y3, ex,ey3 )
    gr3.SetMarkerColor( 3 ), gr3.SetMarkerStyle( 21 ), gr3.SetLineColor( 1 ), gr3.SetLineWidth( 2 )
    mg.Add(gr3)

    # for eta=2.01-2.4
    gr4 = TGraphErrors( n, x, y4, ex,ey4 )
    gr4.SetMarkerColor( 6 ), gr4.SetMarkerStyle( 21 ), gr4.SetLineColor( 1 ), gr4.SetLineWidth( 2 )
    mg.Add(gr4)

    # Drawing multigraph
    mg.Draw( 'AP' )
    mg.SetMaximum(1.8)
    mg.SetMinimum(0.6)
    mg.GetXaxis().SetLimits(20,100)

    # Legend
    leg = ROOT.TLegend(0.11,0.7,0.59,0.89)
    leg.SetBorderSize(0), leg.SetTextFont(42), leg.SetTextSize(0.04)
    leg.SetFillColor(0), leg.SetLineColor(0), leg.SetFillStyle(0)
    leg.AddEntry(gr1,'3-prong & |#eta_{#tau}|=0-0.8',"ep")
    leg.AddEntry(gr2,'3-prong & |#eta_{#tau}|=0.8-1.37',"ep")
    leg.AddEntry(gr3,'3-prong & |#eta_{#tau}|=1.52-2.0',"ep")
    leg.AddEntry(gr4,'3-prong & |#eta_{#tau}|=2.01-2.4',"ep")
    leg.Draw()

    c.Update()
    ROOT.gPad.Modified()
    c.SaveAs('pt_eta_3_prong.pdf')

# Main function
def main():

    #dictionary for 1prong
    table_1 = glob.glob('**/table_nTrack1.csv', recursive=True)[0]
    lines_1prong = read_file(table_1)
    lama_1prong = sort(lines_1prong)
    #dictionary for 3prong
    table_2 = glob.glob('**/table_nTrack3.csv', recursive=True)[0]
    lines_3prong = read_file(table_2)
    lama_3prong = sort(lines_3prong)

    plot_pt_eta_1_prong(lama_1prong)
    plot_pt_eta_3_prong(lama_3prong)

if __name__ == '__main__':
    main()


